package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;


    // tasking story 1.1
    @Test
    void should_return_status_that_is_201() throws Exception {
        mockMvc.perform(post("/api/games")).andExpect(status().is(HttpStatus.CREATED.value()));
    }



    // tasking story 1.2
    @Test
    void should_return_status_that_is_201_and_return_game_id() throws Exception {
        mockMvc.perform(post("/api/games/2"))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(jsonPath("$.gameId").value("2"));

    }

    // tasking story 2.1
    @Test
    void should_return_game_status_that_include_gameid_answer() throws Exception {

        mockMvc.perform(post("/api/games/2"));

        mockMvc.perform(get("/api/games/2"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.answer").exists());
    }

    @Test
    void should_return_status_that_is_404_when_input_no_gameid() throws Exception {
        mockMvc.perform(get("/api/games/3")).andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    void should_correct_answer_when_input_user_conrrect_answer() throws Exception {
        mockMvc.perform(post("/api/games/test/2"));

        mockMvc.perform(patch("/api/games/2").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{\n" +
                "\t\"answer\": \"1234\"\n" +
                "}"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.hint").value(false));
    }

    @Test
    void should_return_user_prompt_when_input_wrong_answer() throws Exception {
        mockMvc.perform(post("/api/games/2"));
        mockMvc.perform(patch("/api/games/2").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{ \"answer\": \"1234\" }\n"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.hint").value(false))
                .andExpect(jsonPath("$.answer").exists());
    }

    @Test
    void test_exception_when_game_id_inconrrect_format() throws Exception {
        mockMvc.perform(get("/api/games/a")).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void test_exception_when_answer_length_little_four() throws Exception {
        mockMvc.perform(post("/api/games/test/2"));
        mockMvc.perform(patch("/api/games/2").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{ \"answer\": \"124\" }\n"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void test_exception_when_answe_have_number_char() throws Exception {
        mockMvc.perform(post("/api/games/test/2"));
        mockMvc.perform(patch("/api/games/2").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{ \"answer\": \"1s24\" }\n"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void test_exception_when_answe_have_other_char() throws Exception {
        mockMvc.perform(post("/api/games/test/2"));
        mockMvc.perform(patch("/api/games/2").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{ \"answer\": \"1s24\" }\n"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }
}
