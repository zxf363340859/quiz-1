package com.twuc.webApp.controller;

import com.twuc.webApp.bean.GameContext;
import com.twuc.webApp.bean.UserPrompt;
import com.twuc.webApp.util.AnswerPrompt;
import com.twuc.webApp.util.ArgumentCheck;
import com.twuc.webApp.util.AutoGenerateAnswer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static com.twuc.webApp.util.ArgumentCheck.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UtilTest {

    //tasking story 2.2

    @Test
    void should_return_string_include_four_dig() {
        assertTrue(AutoGenerateAnswer.generateAnswer() instanceof String);
    }

    @Test
    void test_answer_prompt() {
        String answer = AutoGenerateAnswer.generateAnswer();
        UserPrompt userPrompt = AnswerPrompt.promptAnswer("2222", answer);
        assertNotNull(userPrompt);
    }

    @Test
    void test_util_answer_format_when_answer_length_little_four() {
        checkAnswerFormat("11");
    }

    @Test
    void test_util_answer_format_when_answer_have_other_char() {
        checkAnswerFormat("11qq");
    }

    @Test
    void test_util_game_id_when_game_id_no_contain() {
        Map<String, GameContext> contextContainer = new HashMap<>();
        checkArgument(contextContainer, "1");
    }

    @Test
    void test_util_game_id_when_game_have_other_char() {
        Map<String, GameContext> contextContainer = new HashMap<>();
        checkArgument(contextContainer, "a");
    }
}
