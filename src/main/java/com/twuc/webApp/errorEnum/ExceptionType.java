package com.twuc.webApp.errorEnum;

public enum ExceptionType {

    NO_GAME_ID("404", "gameId不存在"),
    GAME_ID_FORMAT_INCORRECT("400","gameId格式不正确"),
    ANSWER_LITTLE_FOUR("400", "答案小于4"),
    ANSWER_HAVE_OTHER_CHAR("400", "答案含有非数字字符"),
    ANSWER_HAVE_REPEAT_NUM("404", "答案含有重复字符");

    private String status;
    private String message;

    ExceptionType(String status, String message) {
        this.message = message;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
