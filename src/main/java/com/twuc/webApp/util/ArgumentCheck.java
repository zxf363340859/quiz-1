package com.twuc.webApp.util;

import com.twuc.webApp.bean.GameContext;
import com.twuc.webApp.errorEnum.ExceptionType;

import java.util.HashSet;
import java.util.Map;

public class ArgumentCheck {

    public static void checkAnswerFormat(String s) {
        validAnswerLength(s);
        validAnserContent(s);
    }

    private static void validAnswerLength(String s) {
        if(s.length() < 4) {
            throw new NumberFormatException(ExceptionType.ANSWER_LITTLE_FOUR.getStatus());
        }
    }

    private static void validAnserContent(String s) {
        try {
            try {
                Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                throw new NumberFormatException(ExceptionType.ANSWER_HAVE_OTHER_CHAR.getStatus());
            }
            HashSet<Character> characters = new HashSet<>();
            for(int i = 0; i < s.length(); i++) {
                characters.add(s.charAt(i));
            }
            if(characters.size() != s.length()) {
                throw new NumberFormatException(ExceptionType.ANSWER_HAVE_REPEAT_NUM.getStatus());
            }
        } catch (NumberFormatException ex) {
            throw ex;
        }
    }

    public static void checkArgument(Map<String, GameContext> contextContainer, String gameId) {
        validArgumentFormat(gameId);
        isIncludeGameId(contextContainer, gameId);
    }

    public static void validArgumentFormat(String gameId) {
        try {
            Integer.parseInt(gameId);
        }catch (NumberFormatException ex) {
            throw new NumberFormatException(ExceptionType.GAME_ID_FORMAT_INCORRECT.getStatus());
        }
    }

    private static void isIncludeGameId(Map<String, GameContext> contextContainer, String gameId) {
        if(!contextContainer.containsKey(gameId)) {
            throw new NumberFormatException(ExceptionType.NO_GAME_ID.getStatus());
        }
    }

}
