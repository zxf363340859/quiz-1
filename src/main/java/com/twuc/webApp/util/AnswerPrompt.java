package com.twuc.webApp.util;

import com.twuc.webApp.bean.UserPrompt;

public class AnswerPrompt {

    public final static Integer ANSWER_LENGTH = 4;

    public static UserPrompt promptAnswer(String userAnswer, String conrrectAnswer) {
        if(conrrectAnswer.equals(userAnswer)){
            return new UserPrompt(true, conrrectAnswer);
        }

        int aCount = 0;
        int bCount = 0;
        for (int i = 0; i < ANSWER_LENGTH; i++) {
            char c = userAnswer.charAt(0);
            if(conrrectAnswer.contains(c + "") && conrrectAnswer.charAt(i) == c) {
                aCount ++;
            } else if(conrrectAnswer.contains(c + "") && conrrectAnswer.charAt(i) != c) {
                bCount ++;
            }
        }
        return new UserPrompt(false, generatePrompt(aCount, bCount));
    }

    private static String generatePrompt(int aCount, int bCount) {
        return aCount + "A" + bCount + "B";
    }

}
