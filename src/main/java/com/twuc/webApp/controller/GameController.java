package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.bean.GameContext;
import com.twuc.webApp.bean.Location;
import com.twuc.webApp.bean.UserAnswer;
import com.twuc.webApp.errorEnum.ExceptionType;
import com.twuc.webApp.util.AnswerPrompt;
import com.twuc.webApp.util.ArgumentCheck;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.twuc.webApp.util.AutoGenerateAnswer.generateAnswer;

@RestController
public class GameController {

    private static ObjectMapper objectMapper = new ObjectMapper();

    private static Map<String, GameContext> contextContainer = new HashMap<>();


    @PostMapping("/api/games")
    @ResponseBody
    public ResponseEntity createGame() {
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @PostMapping("/api/games/{gameId}")
    @ResponseBody
    public ResponseEntity<String> playGame(@PathVariable String gameId) throws JsonProcessingException {
        ArgumentCheck.validArgumentFormat(gameId);
        contextContainer.put(gameId, new GameContext(gameId, generateAnswer()));
        String writeValueAsString = objectMapper.writeValueAsString(new Location(gameId));
        return ResponseEntity.status(HttpStatus.CREATED).body(writeValueAsString);
    }

    @GetMapping("/api/games/{gameId}")
    @ResponseBody
    public ResponseEntity<String> getGameStatus(@PathVariable String gameId) throws JsonProcessingException {
        ArgumentCheck.checkArgument(contextContainer, gameId);
        String string = objectMapper.writeValueAsString(contextContainer.get(gameId));
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).body(string);
    }


    @PostMapping("/api/games/test/{gameId}")
    @ResponseBody
    public ResponseEntity<String> playGameTest(@PathVariable String gameId) throws JsonProcessingException {
        contextContainer.put(gameId, new GameContext(gameId, generateAnswer()));
        String writeValueAsString = objectMapper.writeValueAsString(new Location(gameId));
        return ResponseEntity.status(HttpStatus.CREATED).body(writeValueAsString);
    }


    @PatchMapping("/api/games/{gameId}")
    @ResponseBody
    public ResponseEntity<String> guessAnswer(@PathVariable String gameId, @RequestBody UserAnswer userAnswer) throws JsonProcessingException {
        ArgumentCheck.checkArgument(contextContainer, gameId);
        ArgumentCheck.checkAnswerFormat(userAnswer.getAnswer());
        String string = objectMapper.writeValueAsString(AnswerPrompt.promptAnswer(userAnswer.getAnswer(), contextContainer.get(gameId).getAnswer()));
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).body(string);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> argumentExceptionHandler() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<String> userAnserExceptionHandler(NumberFormatException ex) {
        if(ex.getMessage().equals(ExceptionType.GAME_ID_FORMAT_INCORRECT.getStatus())) {
            return ResponseEntity.status(400).body(ExceptionType.GAME_ID_FORMAT_INCORRECT.getMessage());
        } else if(ex.getMessage().equals(ExceptionType.NO_GAME_ID.getStatus())) {
            return ResponseEntity.status(404).body(ExceptionType.NO_GAME_ID.getMessage());
        } else if(ex.getMessage().equals(ExceptionType.ANSWER_LITTLE_FOUR.getStatus())) {
            return ResponseEntity.status(400).body(ExceptionType.ANSWER_LITTLE_FOUR.getMessage());
        } else if(ex.getMessage().equals(ExceptionType.ANSWER_HAVE_OTHER_CHAR.getStatus())) {
            return ResponseEntity.status(400).body(ExceptionType.ANSWER_HAVE_REPEAT_NUM.getMessage());
        } else if(ex.getMessage().equals(ExceptionType.ANSWER_HAVE_OTHER_CHAR.getStatus())) {
            return ResponseEntity.status(400).body(ExceptionType.ANSWER_HAVE_REPEAT_NUM.getMessage());
        }
        return ResponseEntity.status(400).body(ex.getMessage());
    }


}
