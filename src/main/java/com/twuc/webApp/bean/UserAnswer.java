package com.twuc.webApp.bean;

public class UserAnswer {

    private String answer;

    public UserAnswer() {
    }

    public UserAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }
}
