package com.twuc.webApp.bean;

public class Location {

    private String gameId;

    public Location() {
    }

    public Location(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return gameId;
    }

}
