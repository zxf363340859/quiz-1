package com.twuc.webApp.bean;

public class UserPrompt {

    private Boolean hint;

    private String answer;

    public UserPrompt() {
    }

    public UserPrompt(Boolean hint, String answer) {
        this.hint = hint;
        this.answer = answer;
    }

    public Boolean getHint() {
        return hint;
    }

    public String getAnswer() {
        return answer;
    }
}
