package com.twuc.webApp.bean;

public class GameContext {

    private String id;
    private String answer;

    public GameContext() {
    }

    public GameContext(String id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public String getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }
}
